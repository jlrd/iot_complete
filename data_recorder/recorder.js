//lets require/import the mongodb native drivers.
var mongodb = require('mongodb');

//We need to work with "MongoClient" interface in order to connect to a mongodb server.
var MongoClient = mongodb.MongoClient;

// Connection URL. This is where your mongodb server is running.
var url = 'mongodb://192.168.1.26:27017/recorder';

var t1 = new Date().getTime();


var mqtt = require('mqtt');
var client = mqtt.connect('mqtt://192.168.1.50:1883');


// Use connect method to connect to the Server
MongoClient.connect(url, function (err, db) {
  if (err) {
    console.log('Unable to connect to the mongoDB server. Error:', err);
  } else {
    //HURRAY!! We are connected. :)
    console.log('Connection established to', url);

    // Get the documents collection
    var collection = db.collection('1131');

    //query data    date_time: {$gt:(new Date('2015-02-04'))}
    /*
    collection.find({ "_id": { $gt: (new Date("2015-02-02")), $lt: (new Date('2015-02-03')) } }).toArray(function (err, result) {
      if (err) {
        console.log(err);
      } else if (result.length) {
        //console.log('Found:', result);
        result.forEach(function(element) {

        }, this);
      } else {
        console.log('No document(s) found with defined "find" criteria!');
      }
    });
*/
    //console.log(new Date().getTime() - t1);

    date = new Date("2015-02-02 18:31:00");
    back_date = new Date((new Date(date)).getTime() - 60 * 60 * 1000);
    //console.log(date);


    /*
    // Insert some users
    collection.update({ _id: { $gte: back_date, $lt: date } }, { $set: { _id: "2015-02-02 18:00:00" } }, function (err, numUpdated) {
      if (err) {
        console.log(err);
      } else if (numUpdated) {
        console.log('Updated Successfully %d document(s).', numUpdated);
      } else {
        console.log('No document found with defined "find" criteria!');
      }

      //Close connection
      db.close();
    });
*/

    //console.log( new Date("2015-02-02 18:30:00"));

    var data_array = []; // $gte: back_date, $lt: date


    /*
        collection.update({}, { $set: { temperature: { temperature } } }, function (err, numUpdated) {
          if (err) {
            console.log(err);
          } else if (numUpdated) {
            console.log('Updated Successfully %d document(s).', numUpdated);
          } else {
            console.log('No document found with defined "find" criteria!');
          }
    
          //Close connection
        });
    */



    client.on('connect', function () {
      console.log("subscribed on +")
      client.subscribe('+');
      //client.publish('presence', 'Hello mqtt');
    });

    client.on('message', function (topic, message) {
      // message is Buffer
      //console.log(topic.toString() + ': ' + message.toString());
      //client.end();
      var json_msg = JSON.parse(message.toString());
      //console.log("ok");
      //console.log();
      process.stdout.write(". ");

      collection.find({ _id: { $lt: (new Date(json_msg.date_time)) } }, { date_time: [] }).toArray(function (err, result) {
        if (err) {
          console.log(err);
          db.close();

        } else if (result.length) {
          console.log("data found!");
          //console.log('Found:', result);
          //console.log(new Date(result[0]["date_time"][1438]));
          console.log("result count:" + result[0]["date_time"].length);
        } else {
          console.log('No document(s) found with defined "find" criteria!');
        }
      });
    });

  }
});

